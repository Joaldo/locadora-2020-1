package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	private static final String PLACA_DEFAULT = "XXXXXXX";
	private static final Integer ANO_DEFAULT = 2000;
	private static final Modelo MODELO_DEFAULT = new Modelo("Popular");
	private static final Double VALORDIARIO_DEFAULT = 800.00;

	private SituacaoVeiculoEnum situacao;
	private String placa = PLACA_DEFAULT;
	private Integer ano = ANO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT;
	private Double valorDiario = VALORDIARIO_DEFAULT;

	public VeiculoBuilder() {
	}
	
	public static VeiculoBuilder umVeiculoBuilder() {
		return new VeiculoBuilder();
	} 
	
	public static VeiculoBuilder veiculo2019() {
		return VeiculoBuilder.umVeiculoBuilder().comAno(2019);
	}
	
	public static VeiculoBuilder veiculo2012() {
		return VeiculoBuilder.umVeiculoBuilder().comAno(2012);
	}


	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder comValor(Double valorDiario) {
		this.valorDiario = valorDiario;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder manutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder mas() {
		return umVeiculoBuilder().comAno(this.ano).comModelo(this.modelo).comPlaca(this.placa)
				.comSituacao(this.situacao).comValor(this.valorDiario);
	}
	
	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setAno(this.ano);
		veiculo.setModelo(this.modelo);
		veiculo.setPlaca(this.placa);
		veiculo.setSituacao(this.situacao);
		veiculo.setValorDiaria(this.valorDiario);
		return veiculo;
	}
	
	public static VeiculoBuilder veiculoDefault() {
		return VeiculoBuilder.umVeiculoBuilder()
				.comAno(ANO_DEFAULT)
				.comModelo(MODELO_DEFAULT)
				.comPlaca(PLACA_DEFAULT)
				.comSituacao(SITUACAO_DEFAULT)
				.comValor(VALORDIARIO_DEFAULT);
	}

}
