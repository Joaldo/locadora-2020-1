package br.ucsal.bes20201.testequalidade.locadora;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o calculo do valor de locação por 3 dias para 2 veiculos com 1 ano de
	 * fabricação e 3 veiculos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
		Veiculo veiculo1 = VeiculoBuilder.veiculo2012().build();
		Veiculo veiculo2 = VeiculoBuilder.veiculo2012().build();
		
		Veiculo veiculo3 = VeiculoBuilder.veiculo2019().build();
		Veiculo veiculo4 = VeiculoBuilder.veiculo2019().build();
		Veiculo veiculo5 = VeiculoBuilder.veiculo2019().build();
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
		
		
		VeiculoDAO.insert(veiculo1);
		VeiculoDAO.insert(veiculo2);
		VeiculoDAO.insert(veiculo3);
		VeiculoDAO.insert(veiculo4);
		VeiculoDAO.insert(veiculo5);
		
		assertEquals(11520.0, LocacaoBO.calcularValorTotalLocacao(veiculos, 3));
	}

}
